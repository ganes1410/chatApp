import pandas as pd
import pickle
import sys


# start = time.time()
train = pd.read_csv('/home/ganesh/Downloads/chatapp/Testing/datasets/traindata.csv')

comment = sys.argv[1] 
X_test = [comment] 

Y_train = train[train.columns[4:]]


loaded_fit = pickle.load(open('/home/ganesh/Downloads/chatapp/Testing/fit_model.sav','rb'))
X_test = loaded_fit.transform(X_test)

for i in range(Y_train.shape[1]):
    feature = Y_train.columns[i]
    loaded_model = pickle.load(open('/home/ganesh/Downloads/chatapp/Testing/model_{}.sav'.format(feature), 'rb'))
    exec('pred_{} = pd.Series(loaded_model.predict_proba(X_test).flatten()[1::2])'.format(feature))

submission = pd.DataFrame({
    #'comment': comment,
    # 'toxic': pred_toxic,
    # 'severe_toxic': pred_severe_toxic,
    'obscene': pred_obscene,
    'threat': pred_threat,
    'insult': pred_insult,
    'identity_hate': pred_identity_hate,
    'none': pred_none
})


submission.to_csv("/home/ganesh/Downloads/chatapp/Testing/result.csv", index=False)
print(submission.idxmax(axis=1).get(0))

# end = time.time()

# print('Total time took = {}'.format(end - start)) 