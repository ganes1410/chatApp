var app = require("express")();
var http = require("http").Server(app);
var io = require("socket.io")(http);
var PythonShell = require("python-shell");

app.get("/", function(req, res) {
  res.sendFile(__dirname + "/index.html");
});
io.on("connection", function(socket) {
  console.log("a user connected");
  socket.on("disconnect", function() {
    console.log("user disconnected");
  });
});
io.on("connection", function(socket) {
  socket.on("chat message", function(msg) {
    var options = {
      mode: "text",
      pythonPath: "/home/ganesh/anaconda3/bin/python",
      pythonOptions: ["-u"], // get print results in real-time
      scriptPath: "/home/ganesh/Downloads/chatapp/Testing",
      args: [msg]
    };

    PythonShell.run("classifier.py", options, function(err, results) {
      if (err) throw err;
      console.log(results[0]);
      io.emit("chat message", { msg, result: results[0] });
    });
  });
});

http.listen(3000, function() {
  console.log("Listening on 3000");
});
